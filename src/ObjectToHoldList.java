import java.util.ArrayList;


public class ObjectToHoldList {

	private ArrayList<Double> lastKnownData = new ArrayList<>();
	
	public ObjectToHoldList(ArrayList<Double> data){
		this.lastKnownData = data;
	}
	public ObjectToHoldList() {
	}
	public void setArray(ArrayList<Double> data){
		this.lastKnownData = data;
	}
	public ArrayList<Double> getData(){
		return lastKnownData;
	}
}
