import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;


public class TableCreator {
	
	public TableView createIndividualTable(final LineChart<String, Number> chart, final TableView table){
		TableColumn legendCol;
		if (!chart.getData().isEmpty()) {
			legendCol = new TableColumn("Legend");			
			legendCol.setResizable(false);
			legendCol.setCellValueFactory(
					new Callback<CellDataFeatures<XYChart.Series<String,Number>, String>, ObservableValue<XYChart.Series<String,Number>>>() {
						@Override public ObservableValue<Series<String, Number>> call(CellDataFeatures<Series<String, Number>, String> param) {
							return new SimpleObjectProperty(param.getValue());
						}
					}
					);
			legendCol.setCellFactory(new Callback<TableColumn<XYChart.Series<String,Number>,Series<String, Number>>,TableCell<XYChart.Series<String,Number>,Series<String, Number>>>() {
				@Override public TableCell<Series<String, Number>, Series<String, Number>> call(TableColumn<Series<String, Number>, Series<String, Number>> param) {
					return new TableCell<Series<String, Number>, Series<String, Number>>() {
						@Override protected void updateItem(Series<String, Number> series, boolean empty) {
							super.updateItem(series, empty);
							if (series != null) {
								setText(series.getName());
								setGraphic(createSymbol(series, chart.getData().indexOf(series)));
							}
						}
					};
				}
			});
			legendCol.setMinWidth(0);
			legendCol.setId("legend");

			legendCol.setText("Serie");;
			table.getColumns().add(legendCol);
			final ObservableList<Data<String, Number>> firstSeriesData = chart.getData().get(0).getData();
			for (final Data<String, Number> item: firstSeriesData) {
				TableColumn col = new TableColumn(item.getXValue());
				col.setSortable(false);
				col.setResizable(false);
				col.setStyle("-fx-font-size: 10;");
				col.prefWidthProperty().bind(chart.getXAxis().widthProperty().divide(firstSeriesData.size()));
				col.setCellValueFactory(
						new Callback<CellDataFeatures<XYChart.Series<String,Number>, String>, ObservableValue<Number>>() {
							@Override public ObservableValue<Number> call(CellDataFeatures<XYChart.Series<String,Number>, String> param) {
								/*
								 *for each loop over alle data i listen 
								 */
								for (Data<String, Number> curItem: param.getValue().getData()) {
									if (curItem.getXValue().equals(item.getXValue())) {
										return curItem.YValueProperty();
									}
								}
								return null;
							}
						}
						);
				col.setMinWidth(30);
				table.autosize();
				table.getColumns().add(col);
			}
			for (XYChart.Series<String,Number> series: chart.getData()) {
				table.getItems().add(series);
			}
			table.setEditable(false);
			table.setFocusTraversable(false);
		}
		/*
		 * sets the table height and y-cordinate - This is used to make the table fit the lineCharts x-Axis
		 */
		table.setTranslateY(-45);
		table.setPrefHeight(100);
		table.setStyle("-fx-box-border: transparent; -fx-focus-color: transparent; -fx-padding: 0 10 0 10;");
		table.getSelectionModel().setCellSelectionEnabled(true);
		table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		MenuItem kopierOverskift = new MenuItem("Kopi�r overskift");
		kopierOverskift.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				String copiedContent = "";
				final ClipboardContent copycontent = new ClipboardContent();
				for (int i = 0; i < Main.getTimeArray().length; i++) {
					copiedContent = copiedContent+ " "+Main.getTimeArray()[i] + "\t";
				}
				copycontent.putString(copiedContent.toString());
				Clipboard.getSystemClipboard().setContent(copycontent);
			}
		});
		MenuItem item = new MenuItem("Kopi�r");
		item.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ObservableList<TablePosition> posList = table.getSelectionModel().getSelectedCells();
				int old_r = -1;
				StringBuilder clipboardString = new StringBuilder();
				for (TablePosition p : posList) {
					int r = p.getRow();
					int c = p.getColumn();
					Object cell = ((TableColumn) table.getColumns().get(c)).getCellData(r);
					if (cell == null)
						cell = "";
					if (old_r == r)
						clipboardString.append('\t');
					else if (old_r != -1)
						clipboardString.append('\n');
					clipboardString.append(cell);
					old_r = r;
				}
				final ClipboardContent content = new ClipboardContent();
				System.out.println(clipboardString);
				content.putString(clipboardString.toString());
				Clipboard.getSystemClipboard().setContent(content);
			}
		});
		MenuItem item2 = new MenuItem("Kopier r�kke");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String rowContent = "";
				ObservableList<TablePosition> posList = table.getSelectionModel().getSelectedCells();
				System.out.println(rowContent);
				for (TablePosition p : posList) {
					int c = 1;
					int row = p.getRow();
					System.out.println("c " +c);
					System.out.println("row "+row);
					for (int i = 1; i < table.getColumns().size(); i++) {
						rowContent = rowContent +" "+((TableColumn) table.getColumns().get(c)).getCellData(row)+"\t";
							c++;			
					}		
				}
				final ClipboardContent allContent = new ClipboardContent();
				allContent.putString(rowContent.toString());
				Clipboard.getSystemClipboard().setContent(allContent);
				System.out.println(allContent.toString());
				rowContent = "";
			}
		});
		ContextMenu menu = new ContextMenu();
		menu.getItems().addAll(kopierOverskift,item,item2);
		table.setContextMenu(menu);
		return table;
		
		
	}
	private Node createSymbol(Series<String, Number> series, int seriesIndex) {
		Node symbol = new StackPane();   
		symbol.getStyleClass().setAll(    
				"chart-line-symbol",    
				"series" + seriesIndex,   
				"default-color" + (seriesIndex % 8)   
				);    return symbol;
	}  
}
