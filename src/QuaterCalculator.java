import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import Time.TimeOfDay;
public class QuaterCalculator {
	private TreeMap<Integer, TreeMap<Integer, Integer>> seperateChaining = new TreeMap<Integer,TreeMap<Integer, Integer>>();
	private ArrayList<Integer> dataOutput = new ArrayList<Integer>();
	private final Integer[] normalOpenHours= {8,9,10,15,16,17,18,19,20};
	private final ArrayList<Integer>normalOpenHoursArrayList=new ArrayList<Integer>();
	private int numberOfWeekends;
	public final int UGE = 2;
	public final int MONTH = 3;
	public final int DAY = 1;
	private boolean percentage;
	private int timeFrameView;
	private int timeFrame;
	public QuaterCalculator(int timeFrame, boolean percentage){
		this.timeFrame = timeFrame;
		this.percentage = percentage;
		if (timeFrame == 0) {
			this.timeFrame = 1;
		}
		for (Integer i : normalOpenHours) {
			normalOpenHoursArrayList.add(i);
		}
	}
	/**
	 * Gets the timeFrame.
	 * @return
	 */
	private int getTimeFrame(){
		return timeFrame;
	}
	public void setTimeFrame(int time){
		this.timeFrame = time;
	}
	public void selectTimeFrameView(int timeFrame){
		this.timeFrameView = timeFrame;
	}
	private int getSelectionTimeFrame(){
		return timeFrameView;
	}
	/**
	 * 	
	 * Converts the TreeMap to an ArrayList. 
	 * The ArrayList will automatically be sorted since the TreeMap by nature is sorted.
	 */
	public void mapToArray(){
		ArrayList<Integer> finalDataOutPut = new ArrayList<Integer>(48);

		for (int j = 8; j < 20; j++) {
			if(seperateChaining.get(j) == null){
				finalDataOutPut.add(0);
				finalDataOutPut.add(0);
				finalDataOutPut.add(0);
				finalDataOutPut.add(0);
			}else {
				@SuppressWarnings("rawtypes")
				Iterator it = seperateChaining.get(j).entrySet().iterator();
				int k = 0;
				while (it.hasNext()) {
					@SuppressWarnings("rawtypes")
					Map.Entry pairs = (Map.Entry)it.next();
					if (k == 0 && (int) pairs.getKey() != 0) {
						finalDataOutPut.add(0);
						int value = (int) pairs.getValue();
						finalDataOutPut.add(calulateAverage(value, j));
						k++;
					}else {
						finalDataOutPut.add((int)calulateAverage((int)pairs.getValue(), j));
						k++;
					}
					if (k == 4) {
						k = 0;
					}
				}
				//System.out.println(j+" "+pairs.getKey() + " = " + pairs.getValue());
			}
		}
		finalDataOutPut.add(0);
		if (getTimeFrame() > 7) {
		}
		this.dataOutput = finalDataOutPut;
	}
	/**
	 * separate chaining - This method adds data to the 2d HASHMAP using the principles of separate chaining!
	 * @param decimalHour
	 * @param callAmount
	 */
	public void addToList(double decimalHour, int callAmount){
		// converts the search key to hour
		int searchingKey = (int)(24*decimalHour);
		if (checkForNewHourStart(decimalHour)) {
			searchingKey++;
		}
		// if the searchkey is 7 (which exists in once case (07:59889) or decimal (0.3333) then add 0.001 to searchkey to convert it to 8:00
		if (searchingKey == 7) {
			decimalHour += 0.001;
			searchingKey = (int)(24*decimalHour);
		}
		//if the convertet decimal is 60 then add 1 to hour and set minutes to 0
		// if the hashmap contains the hour already
		if (seperateChaining.containsKey(searchingKey)) {
			// convert the decimal to minutes
			int convertDecimalToQuater = convertDecimalToQuaterLevel(decimalHour);
			// if the hashmap's hashmap contains a value on the minute mark
			if (seperateChaining.get(searchingKey).containsKey(convertDecimalToQuater)){
				int oldValue = seperateChaining.get(searchingKey).get(convertDecimalToQuater);
				// add new value to minute mark
				seperateChaining.get(searchingKey).put(convertDecimalToQuater, oldValue + callAmount);
			}else {
				// else add value to minute mark
				seperateChaining.get(searchingKey).put(convertDecimalToQuater, callAmount);
			}
			// if the hashmap does not contain a value at the key
		}else {
			// create a new hashmap on the key
			seperateChaining.put(searchingKey, new TreeMap<Integer, Integer>());
			int convertDecimalToQuater = convertDecimalToQuaterLevel(decimalHour);
			// add value to hashmap's hashmap
			seperateChaining.get(searchingKey).put(convertDecimalToQuater, callAmount);
		}	
	}
	/**
	 * Checks if the decimal hour converted to minutes is = x.590998 (or closer) 
	 * returns true if.
	 * @param decimalHour
	 * @return
	 */
	private boolean checkForNewHourStart(double decimalHour) {
		int actualHour = (int)((24*decimalHour));
		double minutes = 0;
		minutes = ((24*decimalHour) - actualHour);
		minutes = minutes*60;	
		minutes = Math.round(minutes);
		if (actualHour == 10) {
		}
		return minutes == 60;
	}
	/**
	 * This method converts the decimal into minutes
	 * @param decimalHour
	 * @return
	 */
	private int convertDecimalToQuaterLevel(double decimalHour) {
		int actualHour = (int)((24*decimalHour));
		double minutes = 0;
		minutes = ((24*decimalHour) - actualHour);
		minutes = minutes*60;	
		minutes = Math.round(minutes);
		if (minutes > 59 || minutes <= 13) {
			minutes = 0;
		}else if (minutes >= 14 && minutes <29) {
			minutes = 15;
		}else if (minutes >= 30 && minutes < 44) {
			minutes = 30;
		}else if (minutes >= 45 && minutes <= 59) {
			minutes = 45;
		}

		return (int) minutes;
	}
	/** 
	 * this method searches the list
	 * @deprecated
	 * @param decimal
	 * @return
	 */
	public int getDataFromlist(double decimal){
		int hour = (int) Math.round(24*decimal);
		int quater= (int) (decimal * 60);
		if (seperateChaining.get(hour).get(quater) == null) {
			return 0;	
		}else {
			return seperateChaining.get(hour).get(quater);
		}
	}
	/**
	 * Gets the final ArrayList that can be used to insert data into the chart
	 * @return
	 */
	public ArrayList<Integer> getFinalDataList(){
		return dataOutput;
	}
	/**
	 * Divides the value with the days between (To get the average pr day)
	 * @param value
	 * @param time
	 * @return
	 */
	private Integer calulateAverage(int value,int time) {
		switch (getSelectionTimeFrame()) {
		// day
		case 1:
			return value;
			//week
		case 2:
			if (normalOpenHoursArrayList.contains(time) ) {
				int divisor;
				if (getTimeFrame() == 4) {
					divisor = getTimeFrame() +1 ;
				}else {
					divisor = 5;
				}
				double d = value / divisor;
				return (int) d;
			}else {
				return (int) value / getTimeFrame();
			}
			//month
		case 3:
			if (normalOpenHoursArrayList.contains(time)) {
				int divisor = getTimeFrame() - numberOfWeekends;
				if (time == 9) {
				}
				return (int) value / divisor;
			}else {
				return (int) value / getTimeFrame();
			}
		default:
			break;
		}
		return (int) value / getTimeFrame();
	}
	/**
	 * gets the original treeMap before data has been manipulated
	 * @return
	 */
	public TreeMap<Integer, TreeMap<Integer, Integer>> getOrigin(){
		return seperateChaining;
	}
	public void clearAll(){
		seperateChaining.clear();
		dataOutput.clear();
	}
	public void setNumberOfWeekends(DateTime startDate, DateTime endDate) {
		int numberOfWeekends = 0;
		while (startDate.getDayOfMonth() != endDate.getDayOfMonth()) {
			startDate = startDate.plusDays(1);
			if (startDate.getDayOfWeek() == DateTimeConstants.SATURDAY || startDate.getDayOfWeek() == DateTimeConstants.SUNDAY) {
				numberOfWeekends++;
			}
		}
		this.numberOfWeekends = numberOfWeekends;
	}
	public void setNumberOfWeekends(int i ) {
		this.numberOfWeekends = i;
	}
	public ArrayList<Double> convertMapToCalculatedArray(Map<TimeOfDay, Integer> mapToConvert){
		ArrayList<Double> finalDataOutPut = new ArrayList<Double>(48);
		Iterator it = mapToConvert.entrySet().iterator();
		TreeMap<Integer, TreeMap<Integer, Integer>> realMap = new TreeMap<Integer, TreeMap<Integer,Integer>>();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pairs = (Map.Entry)it.next();
			int value = (int) pairs.getValue();
			TimeOfDay key = (TimeOfDay) pairs.getKey();
			if (realMap.containsKey(key.getHours())) {
				realMap.get(key.getHours()).put(key.getMinutes(), (int)pairs.getValue());
			}else {
				realMap.put(key.getHours(), new TreeMap<Integer, Integer>());
				realMap.get(key.getHours()).put(key.getMinutes(), (int)pairs.getValue());
			}
		}
		for (int j = 8; j < 20; j++) {
			if(realMap.get(j) == null){
				finalDataOutPut.add((double) 0);
				finalDataOutPut.add((double) 0);
				finalDataOutPut.add((double) 0);
				finalDataOutPut.add((double) 0);
			}else {
				@SuppressWarnings("rawtypes")
				Iterator it2 = realMap.get(j).entrySet().iterator();
				int k = 0;
				while (it2.hasNext()) {
					@SuppressWarnings("rawtypes")
					Map.Entry pairs = (Map.Entry)it2.next();
					if (k == 0 && (int) pairs.getKey() != 0) {
						finalDataOutPut.add((double) 0);
						int value = (int) pairs.getValue();
						finalDataOutPut.add((double)calulateAverage(value, j));
						k++;
					}else {
						finalDataOutPut.add((double)calulateAverage((int)pairs.getValue(), j));
						k++;
					}
					if (k == 4) {
						k = 0;
					}
				}
				//System.out.println(j+" "+pairs.getKey() + " = " + pairs.getValue());
			}
		}
		finalDataOutPut.add((double) 0);
		if (getTimeFrame() > 7) {
		}
	//	finalDataOutPut.add((double)calulateAverage(value,key.getHours()));



		return finalDataOutPut;
	}}

