
import java.util.ArrayList;
import java.util.TreeMap;

import javax.management.Query;

import org.joda.time.DateTime;

import Time.TimeOfDay;
import Time.TimeOutOfBoundsException;
import DopeConnection.DopeDBException;
import DopeConnection.DopeResultSetException;
import DopeConnection.KSDriftConnection;
import DopeConnection.OracleFormats;
import DopeConnection.SqlQuery;

/**
 * This is the provider class. 
 * This class gets the information from the Database and returns it as objects!
 * Also when creating the object it will automaticly set the data.
 * @author MRCR
 *
 */
public class Provider {
	private KSDriftConnection conn; 

	private ArrayList<Double> decimalHourOutput = new ArrayList<Double>();
	private ArrayList<Integer> callAmount = new ArrayList<Integer>();
	public Provider() throws DopeDBException{
		initSql();
	}
	private void initSql() throws DopeDBException {
		try {
			conn = new KSDriftConnection();
		} catch (DopeDBException e) {
			throw e;
		}
	}
	public ArrayList<DateTime> getOpeningDaysInSelectedPeriod(DateTime start, DateTime end){
		ArrayList<DateTime> openingDays = new ArrayList<DateTime>();
		SqlQuery query = null;
		DateTime d = new DateTime();
		query = new SqlQuery(conn,
				"SELECT THIS_DATE FROM KS_DRIFT.SYS_DATE_KS " +
				"WHERE THIS_DATE BETWEEN "+OracleFormats.convertDate(start.toDate()) +
				"AND "+OracleFormats.convertDate(end.toDate())+ "ORDER BY THIS_DATE");

		try {
			query.execute();
			while (query.next()) {
				String dato = query.getString("THIS_DATE");
				String year = dato.substring(0,4);
				String month = dato.substring(5,7);
				String date = dato.substring(8,10);
				//System.out.println(year +" "+ month +" "+ date);
				
				//System.out.println(query.getString("THIS_DATE"));
				DateTime d2 = new DateTime(query.getDateTime("THIS_DATE"));
			//	DateTime d = new DateTime(query.getString("THIS_DATE"));
				openingDays.add(d2);
			}
		} catch (DopeResultSetException | DopeDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return openingDays;
	}
	/** 
	 * The following method is only used as a test method and should be deleted after testing.
	 */
	public ArrayList<Integer> phoneQueueWithWeekends(DateTime startDate, DateTime endDate, String name, QuaterCalculator qc, String notInDays) throws DopeDBException, DopeResultSetException{
		SqlQuery query = null;
		callAmount.clear();
		decimalHourOutput.clear();
		if (name.equalsIgnoreCase("Gns Antal Kald")) {
			query = new SqlQuery(conn, 
					"SELECT TRUNC(TID, 4) AS TID, SUM(ANTAL_KALD) AS ANTAL_KALD FROM KS_DRIFT.PERO_NKM_K�_KVART WHERE DATO " +
							"BETWEEN "+OracleFormats.convertDate(startDate.toDate())+
							"AND "+OracleFormats.convertDate(endDate.toDate())+
							" AND K� NOT IN ('PrivatOverflow','VIPcenterkald','VIPservice')"+" AND DATO NOT IN ("+notInDays+")"+
							" GROUP BY TRUNC(TID, 4)"
					);

		}else if (name.equalsIgnoreCase("procent")) {
			query = new SqlQuery(conn, 
					"SELECT TRUNC(TID, 4) AS TID, SUM(BESVARET_25_SEK) AS ANTAL_KALD FROM KS_DRIFT.PERO_NKM_K�_KVART WHERE DATO " +
							"BETWEEN "+OracleFormats.convertCalendar(startDate.toGregorianCalendar())+
							"AND "+OracleFormats.convertCalendar(endDate.toGregorianCalendar())+
							" AND K� NOT IN ('PrivatOverflow','VIPcenterkald','VIPservice')"+" AND DATO NOT IN ("+notInDays+")"+
							" GROUP BY TRUNC(TID, 4)"
					);	
		}
		query.execute();
		while (query.next()) {
			double decimalTime = query.getDouble("TID");
			decimalHourOutput.add(decimalTime);
			int callAmount = query.getInteger("ANTAL_KALD");
			this.callAmount.add(callAmount);

		}

		/**
		 * This next bit may be deleted at a later point the area is only for testing.
		 * 
		 */
		qc.clearAll();
		int i = 0;
		for (double d : decimalHourOutput) {
			qc.addToList(d, callAmount.get(i));
			i++;
		}
		qc.mapToArray();
		return qc.getFinalDataList();

	}
	public ArrayList<Integer> phoneQueueWithoutWeekends(DateTime startDate2, DateTime endDate2, String name, String notInWeekendsSQL, QuaterCalculator qc, String notInDays) throws DopeDBException, DopeResultSetException{
		SqlQuery query = null;
		callAmount.clear();
		decimalHourOutput.clear();
		if (name.equalsIgnoreCase("Gns Antal kald")) {
			query = new SqlQuery(conn, 
					"SELECT TRUNC(TID, 4) AS TID, SUM(ANTAL_KALD) AS ANTAL_KALD FROM KS_DRIFT.PERO_NKM_K�_KVART WHERE DATO " +
							"BETWEEN " +OracleFormats.convertCalendar(startDate2.toGregorianCalendar())+
							" AND " + OracleFormats.convertCalendar(endDate2.toGregorianCalendar())+
							" AND DATO NOT IN ("+notInWeekendsSQL+")"+
							" AND K� NOT IN ('PrivatOverflow','VIPcenterkald','VIPservice')"+" AND DATO NOT IN ("+ notInDays+")"+
							" GROUP BY TRUNC(TID, 4)"
					);
		}else if (name.equalsIgnoreCase("procent"))  {
			query = new SqlQuery(conn, 
					"SELECT TRUNC(TID, 4) AS TID, SUM(BESVARET_25_SEK)AS ANTAL_KALD FROM KS_DRIFT.PERO_NKM_K�_KVART WHERE DATO " +
							"BETWEEN "+OracleFormats.convertCalendar(startDate2.toGregorianCalendar()) +
							" AND "+OracleFormats.convertCalendar(endDate2.toGregorianCalendar())+ " AND DATO NOT IN ("+notInWeekendsSQL+
							") AND K� NOT IN ('PrivatOverflow','VIPcenterkald','VIPservice')"+" AND DATO NOT IN ("+notInDays+
							") GROUP BY TRUNC(TID, 4)"
					);	
		}	

		query.execute();
		while (query.next()) {
			double decimalTime = query.getDouble("TID");
			decimalHourOutput.add(decimalTime);
			int callAmount = query.getInteger("ANTAL_KALD");
			this.callAmount.add(callAmount);

		}
		/* Outside the foreach loop, checks if the boolean isNew is true if it is create a new object and insert into the list*/

		qc.clearAll();
		int i = 0;
		for (double d : decimalHourOutput) {
			qc.addToList(d, callAmount.get(i));
			i++;
		}
		qc.mapToArray();
		return qc.getFinalDataList();

	}

	public ArrayList<Double> getPrognose (DateTime startDate, DateTime endDate, QuaterCalculator qc, String notInWeekends){
		/*
		 * 8.00 - 1
		 * 8.15 - 2
		 * ...
		 * ..
		 * 
		 */
		TreeMap<TimeOfDay, Integer> dataOutPutMap = new TreeMap<TimeOfDay, Integer>();
		if (notInWeekends == null) {
			notInWeekends = "";
		}
		SqlQuery query = null;
		query = new SqlQuery(conn, "SELECT INTERVAL_START, SUM(INTERACTION_VOLUME) AS VALUE " +
				"FROM KS_DRIFT.WM_FORECAST_DATA " +
				"WHERE FORECAST_DATE BETWEEN "+
				OracleFormats.convertDate(startDate.toDate()) +
				" AND "+OracleFormats.convertDate(endDate.toDate()) + " AND FORECAST_DATE NOT IN ("+notInWeekends+") GROUP BY INTERVAL_START ORDER BY INTERVAL_START");
		try {
			query.execute();
		} catch (DopeDBException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			while (query.next()) {
				try {
					dataOutPutMap.put(new TimeOfDay(query.getDouble("INTERVAL_START")),query.getInteger("VALUE"));
				} catch (TimeOutOfBoundsException | DopeResultSetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (DopeResultSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(dataOutPutMap);
		qc.convertMapToCalculatedArray(dataOutPutMap);
		return qc.convertMapToCalculatedArray(dataOutPutMap);
	}
	public ArrayList<Double> getPrognoseWithWeekend (DateTime startDate, DateTime endDate, QuaterCalculator qc){
		/*
		 * 8.00 - 1
		 * 8.15 - 2
		 * ...
		 * ..
		 * 
		 */
		TreeMap<TimeOfDay, Integer> dataOutPutMap = new TreeMap<TimeOfDay, Integer>();

		SqlQuery query = null;
		query = new SqlQuery(conn, "SELECT INTERVAL_START, SUM(INTERACTION_VOLUME) AS VALUE " +
				"FROM KS_DRIFT.WM_FORECAST_DATA " +
				"WHERE FORECAST_DATE BETWEEN "+
				OracleFormats.convertDate(startDate.toDate()) +
				" AND "+OracleFormats.convertDate(endDate.toDate()) +" GROUP BY INTERVAL_START ORDER BY INTERVAL_START");
		try {
			query.execute();
		} catch (DopeDBException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			while (query.next()) {
				try {
					dataOutPutMap.put(new TimeOfDay(query.getDouble("INTERVAL_START")),query.getInteger("VALUE"));
				} catch (TimeOutOfBoundsException | DopeResultSetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (DopeResultSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		qc.convertMapToCalculatedArray(dataOutPutMap);
		return qc.convertMapToCalculatedArray(dataOutPutMap);
	}
}


