package Time;


public class TimeOfDay implements Comparable<TimeOfDay> {
	
	private int minutes;
	private int hours;
	
	public void setMinutes(int minutes) throws TimeOutOfBoundsException{
		if (minutes > 59) throw new TimeOutOfBoundsException("Minutes cannot be set to a value over 59. Attempted: " + minutes);
		if (minutes < 0) throw new TimeOutOfBoundsException("Minutes cannot be set to a value under 0. Attempted: " + minutes);
		this.minutes = minutes;
	}
	public int getMinutes(){
		return this.minutes;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof TimeOfDay)) return false;
		
		TimeOfDay TimeToCompare = (TimeOfDay) obj;
		
		return (this.hours == TimeToCompare.getHours() && this.minutes == TimeToCompare.getMinutes());
	}
	@Override
	public int hashCode() {
		return ((this.minutes + 12) * 30 + (this.hours + 24) * 20); 
	}
	@Override
	public String toString() {
		return ((this.hours < 10 ? "0" + this.hours : this.hours) + ":" + (this.minutes < 10 ? "0" + this.minutes : this.minutes));
	}
	public void setHours(int hours) throws TimeOutOfBoundsException{
		if (hours > 23) throw new TimeOutOfBoundsException("Hours cannot be set to a value over 23. Attempted: " + minutes);
		if (hours < 0) throw new TimeOutOfBoundsException("Hours cannot be set to a value under 0. Attempted: " + minutes);
		this.hours = hours;
	}
	public int getHours(){
		return this.hours;
	}
	
	public double getTimeAsDecimal(){
		double hours = this.hours;
		double minutes = ((double)this.minutes / (double)60);
		
		return hours + minutes;
	}
	public int getTimeAsMinutes(){
		return (60 * this.hours + this.minutes);
	}
	
	public static TimeOfDay getMinTime(){
		try {
			return new TimeOfDay(0, 0);
		} catch (TimeOutOfBoundsException e) {e.printStackTrace();}
		
		return null;
	}
	public static TimeOfDay getMaxTime(){
		try {
			return (new TimeOfDay(23, 59));
		} catch (TimeOutOfBoundsException e) {e.printStackTrace();}
		
		return null;
	}
	
	public TimeOfDay getTimeFromOffset(int hours, int minutes) throws TimeOutOfBoundsException{
		return new TimeOfDay(this.hours + hours, this.minutes + minutes);
	}
	
	public TimeOfDay(int hours, int minutes) throws TimeOutOfBoundsException{
		this.addHours(hours);
		this.addMinutes(minutes);
	}
	public TimeOfDay(double timeOfDayHours) throws TimeOutOfBoundsException{
		int hours = (int) timeOfDayHours;
		int minutes = (int)((timeOfDayHours - hours) * 60);
		
		this.addHours(hours);
		this.addMinutes(minutes);
	}
	public TimeOfDay(int timeOfDayMinutes) throws TimeOutOfBoundsException{
		this(((double) timeOfDayMinutes / (double) 60));
	}
	
	public void addMinutes(int minutes) throws TimeOutOfBoundsException{
		int newMinutes = (this.minutes + minutes);
	
		if (newMinutes > 59){
			int hoursToAdd = (newMinutes / 60);
			this.addHours(hoursToAdd);
			newMinutes = (newMinutes % 60);
		}
		
		this.minutes = newMinutes;
	}
	public void subtractMinutes(int minutes) throws TimeOutOfBoundsException{
		if (minutes > this.minutes){
			int hoursToSubtract = (minutes / 60);
			int newMinutes = (minutes % 60);
			
			this.subtractHours(hoursToSubtract);
			this.minutes = newMinutes; 
		} else {
			this.minutes -= minutes;
		}
	}
	public void addHours(int hours) throws TimeOutOfBoundsException{
		if ((this.hours + hours) > 23)
			throw new TimeOutOfBoundsException("Time of day cannot be later than 23:59");
		
		this.hours += hours;
	}
	public void subtractHours(int hours) throws TimeOutOfBoundsException{
		if ((this.hours - hours) < 0)
			throw new TimeOutOfBoundsException("Time of day cannot be sooner than 00:00");
		
		this.hours -= hours;
	}
	
	public void addTime(TimeOfDay time) throws TimeOutOfBoundsException{
		TimeOfDay newTime = this.clone();
		
		newTime.addMinutes(time.getMinutes());
		newTime.addHours(time.getHours());
		
		this.addMinutes(time.getMinutes());
		this.addHours(time.getHours());
	}
	public void subtractTime(TimeOfDay time) throws TimeOutOfBoundsException{
		TimeOfDay newTime = this.clone();
		
		newTime.subtractMinutes(time.getMinutes());
		newTime.subtractHours(time.getHours());
		
		this.setHours(newTime.getHours());
		this.setMinutes(newTime.getMinutes());	
	}
	
	public TimeOfDay clone(){
		try {
			return new TimeOfDay(this.hours, this.minutes);
		} catch (TimeOutOfBoundsException e) {
			//Never thrown since values are already validated.
			return null;
		}
	}
	
	@Override
	public int compareTo(TimeOfDay o) {
		if (o == null)
			return 1;
		
		return Double.compare(this.getTimeAsDecimal(), o.getTimeAsDecimal());
	}
	
	public boolean before(TimeOfDay time){
		return (this.getTimeAsDecimal() < time.getTimeAsDecimal());
	}
	public boolean after(TimeOfDay time){
		return (this.getTimeAsDecimal() > time.getTimeAsDecimal());
	}
}