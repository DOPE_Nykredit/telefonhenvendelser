package DopeConnection;


public abstract class DopeException extends Exception{

	private static final long serialVersionUID = 1L;

	private String dopeErrorMsg;
	
	public String getDopeErrorMsg(){
		return this.dopeErrorMsg;
	}
	
	protected DopeException(Exception e, String exceptionMsg, String dopeErrorMsg){
		super(exceptionMsg, e);
		this.dopeErrorMsg = dopeErrorMsg;
	}
}