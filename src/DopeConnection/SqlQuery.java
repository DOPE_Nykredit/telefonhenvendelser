package DopeConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

public class SqlQuery extends SqlStatement {

	private ResultSet results;
	
	public ResultSet getResults(){
		return this.results;
	}
	
	public SqlQuery(DopeDBConnection conn, String query) {
		super(conn, query);
	}
	
	public synchronized void execute() throws DopeDBException{
		if(super.conn.isClosed())
			super.conn.openConnection();
		
		this.results = conn.executeQuery(super.sql);
	}
	
	public boolean next() throws DopeResultSetException {
		try {
			return this.results.next();
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "Cannot move to next in resultset.", "Fejl ved indl�sning af data fra database.");
		}
	}
	
	public void Close() throws DopeDBException{
		try {
			this.results.close();
		} catch (SQLException e) {
			throw new DopeDBException(e, "Could not close resultset.", "Fejl ved fors�g p� lukning af resultset.");
		}
	}
	
	private int getColumnIndex(String columnLabel) throws DopeResultSetException {
		try {
			return this.results.findColumn(columnLabel);
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "ColumnLabel could not be found in resultSet. ColumnLabel: " + columnLabel, "Fejl ved indl�sning af data fra database. Angivet kollonnenavn kunne ikke findes.");
		}
	}
	private void validateColumnIndex(int columnIndex) throws DopeResultSetException {
		if (this.results == null)
			throw new DopeResultSetException("Query not executed.", "Foresp�rgsel ikke eksekveret.");
		
		try {
			if (this.results.getMetaData().getColumnCount() < columnIndex)
				
				throw new DopeResultSetException("ColumnIndex out of bounds.", "Fejl ved indl�sning af data fra database. Forespurgte kollonne er over gr�nsen.");
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "Error retrieving columnCount from resultset.", "Fejl ved indl�sning af data fra database.");
		}
	}
	
	public Object getObject(int columnIndex) throws DopeResultSetException {		
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getObject(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "Error retrieving columnValue from resultset", "Fejl ved indl�sning af data fra database. Kunne ikke indl�se objekt.");
		}
	}
	
	public String getString(String columnLabel) throws DopeResultSetException {
		return this.getString(this.getColumnIndex(columnLabel));
	}
	public String getString(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getString(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex + ". Type is not of String.", "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}
	
	public Integer getInteger(String columnLabel) throws DopeResultSetException {
		return	this.getInteger(this.getColumnIndex(columnLabel));
	}
	public Integer getInteger(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getInt(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}			
	}
	
	public Double getDouble(String columnLabel) throws DopeResultSetException {
		return this.getDouble(this.getColumnIndex(columnLabel));
	}
	public Double getDouble(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getDouble(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}

	public Float getFloat(String columnLabel) throws DopeResultSetException {
		return this.getFloat(this.getColumnIndex(columnLabel));
	}
	public Float getFloat(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getFloat(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}
	
	public Boolean getBoolean(String columnLabel) throws DopeResultSetException {
		return this.getBoolean(this.getColumnIndex(columnLabel));
	}
	public Boolean getBoolean(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getBoolean(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}

	public Byte getByte(String columnLabel) throws DopeResultSetException {
		return this.getByte(this.getColumnIndex(columnLabel));
	}
	public Byte getByte(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getByte(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}

	public Date getDate(String columnLabel) throws DopeResultSetException {
		return this.getDate(this.getColumnIndex(columnLabel));
	}
	public Date getDate(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getDate(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}
	
	public Date getDateTime(String columnLabel) throws DopeResultSetException {
		return this.getDateTime(this.getColumnIndex(columnLabel));
	}
	public Date getDateTime(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getTimestamp(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}
	
	public Date getTime(String columnLabel) throws DopeResultSetException {
		return this.getTime(this.getColumnIndex(columnLabel));
	}
	public Date getTime(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getTime(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indl�sning af data fra database. Forkert datatype.");
		}
	}
	
	public Calendar getCalendar(String columnLabel) throws DopeResultSetException {
		return this.getCalendar(this.getColumnIndex(columnLabel));
	}
	public Calendar getCalendar(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		Calendar c = Calendar.getInstance();
		c.setTime(this.getDateTime(columnIndex));
		return c;
	}
}