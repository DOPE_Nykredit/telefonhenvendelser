package DopeConnection;
public interface ISqlListener {

	public void executed();
	public void executionFailed(DopeDBException e);
	
}
