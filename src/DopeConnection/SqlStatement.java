package DopeConnection;
public abstract class SqlStatement implements Runnable {

	protected String sql;
	protected DopeDBConnection conn;
	
	protected ISqlListener listener;
	
	protected SqlStatement(DopeDBConnection conn, String sql){
		if (conn == null) throw new IllegalArgumentException("Connection cannot be null.");
		if (sql == null) throw new IllegalArgumentException("Query cannot be null.");
		
		this.conn = conn;
		this.sql = sql;
	}
	
	public abstract void execute() throws DopeDBException;
	
	@Override
	public void run() {
		try {
			this.execute();
			
			if (this.listener != null)
				this.listener.executed();
		} catch (DopeDBException e) {
			if (this.listener != null)
				this.listener.executionFailed(e);
		}
		
	}
	
	public void setListener(ISqlListener listner){
		this.listener = listner;
	}
}
