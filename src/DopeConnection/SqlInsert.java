package DopeConnection;
import java.sql.ResultSet;

public class SqlInsert extends SqlStatement {

	private ResultSet generatedKeys;
	private String[] returnColumns;
	
	public ResultSet getGeneratedKeys(){
		return this.generatedKeys;
	}
	
	public SqlInsert(DopeDBConnection conn, String insert) {
		super(conn, insert);
	}
	public SqlInsert(DopeDBConnection conn, String insert, String[] returnColumns) {
		super(conn, insert);
		this.returnColumns = returnColumns;
	}

	@Override
	public void execute() throws DopeDBException {
	    this.generatedKeys = super.conn.executeInsertStatement(super.sql, this.returnColumns);
	}	
}
