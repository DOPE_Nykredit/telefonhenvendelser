package DopeConnection;
public class SqlUpdate extends SqlStatement {

	private int updatedRowsCount;
	
	public int getUpdatedRowsCount(){
		return this.updatedRowsCount;
	}
	
	public SqlUpdate(DopeDBConnection conn, String update) {
		super(conn, update);
	}

	@Override
	public void execute() throws DopeDBException {
		this.updatedRowsCount = super.conn.executeUpdateStatement(super.sql);
	}
}
