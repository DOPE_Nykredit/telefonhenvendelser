package DopeConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class OracleFormats {

	public static String convertDate(java.util.Date dateToConvert){
		if (dateToConvert == null)
			return "null";
		else
			return OracleFormats.convertString(new SimpleDateFormat("dd-MM-yyyy").format(dateToConvert));
	}
	public static String convertCalendar(Calendar calendarToConvert){
		if (calendarToConvert == null)
			return "null";
		else
			return OracleFormats.convertDate(calendarToConvert.getTime());
	}
	public static String convertString(String stringToConvert){
		if (stringToConvert == null)
			return "null";
		else
			return "'" + stringToConvert + "'";
	}
}