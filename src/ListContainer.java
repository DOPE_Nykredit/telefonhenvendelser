import java.util.ArrayList;


public class ListContainer {

	private ArrayList<Double> averageData = new ArrayList<>();
	private ArrayList<Double> percentage = new ArrayList<>();
	private ArrayList<Double> callList = new ArrayList<>();
	private ArrayList<Integer> besvaret25Seklist = new ArrayList<Integer>();
	
	public ListContainer(){
		
	}
	public void setAverageList(ArrayList<Double> a){
		this.averageData = a;
	}
	public ArrayList<Double>getAverageData(){
		return averageData;
	}
	public void setPercentage(ArrayList<Double> percentage){
		this.percentage = percentage;
	}
	public ArrayList<Double>getPercentageList(){
		return percentage;
	}
	public void setCallList(ArrayList<Double> callList){
		this.callList = callList;
	}
	public ArrayList<Double> getCallList(){
		return callList;
	}
	public void clearAllLists(){
		averageData.clear();
		percentage.clear();
		callList.clear();
	}
	public ArrayList<Integer> getBesvaret25Seklist() {
		return besvaret25Seklist;
	}
	public void setBesvaret25Seklist(ArrayList<Integer> besvaret25Seklist) {
		this.besvaret25Seklist = besvaret25Seklist;
	}
}
