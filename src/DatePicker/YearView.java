package DatePicker;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

/**
 * The year view shows the months.
 *
 * @author Marc Rasmussen
 */
final class YearView extends DatePane {

	private static final String CSS_CALENDAR_YEAR_VIEW = "calendar-year-view";
	private static final String CSS_CALENDAR_MONTH_BUTTON = "calendar-month-button";


	public YearView(final CalendarView calendarView, final DatePicker d) {
		super(calendarView,d);

		getStyleClass().add(CSS_CALENDAR_YEAR_VIEW);

		// When the locale changes, update the contents (month names).
		calendarView.localeProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				updateContent();
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void buildContent() {

		// Get the number of months. I read, there are some lunar calendars, with more than 12 months.
		final int numberOfMonths = calendarView.getCalendar().getMaximum(Calendar.MONTH) + 1;

		int numberOfColumns = 3;

		for (int i = 0; i < numberOfMonths; i++) {
			final int j = i;
			final Button button = new Button();
			button.getStyleClass().add(CSS_CALENDAR_MONTH_BUTTON);

			// Make the button stretch.
			button.setMaxWidth(Double.MAX_VALUE);
			button.setMaxHeight(Double.MAX_VALUE);
			GridPane.setVgrow(button, Priority.ALWAYS);
			GridPane.setHgrow(button, Priority.ALWAYS);

			button.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent actionEvent) {

					if (calendarView.currentlyViewing.get() == Calendar.YEAR) {
						calendarView.getCalendar().set(Calendar.MONTH, j);
						calendarView.currentlyViewing.set(Calendar.MONTH);
						calendarView.calendarDate.set(calendarView.getCalendar().getTime());                
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(new Date());
						calendar.set(Calendar.DATE, 1);
						calendar.set(Calendar.MONTH,j);
						calendar.set(Calendar.YEAR, calendarView.getCalendar().get(Calendar.YEAR));
						calendar.set(Calendar.HOUR_OF_DAY, 0);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);
						if (!d.getSelection().equalsIgnoreCase("UGE")) {
							calendarView.selectedDate.set(calendar.getTime());						
						}
		
						if (d.getSelection().equalsIgnoreCase("M�ned")) {
							DateTime dt = new DateTime(calendar.getTime());
							dt.monthOfYear().getAsString();
							d.setTextFieldText(dt.monthOfYear().getAsText());
							
						}
					}           
				}
			});
			int rowIndex = i % numberOfColumns;
			int colIndex = (i - rowIndex) / numberOfColumns;
			add(button, rowIndex, colIndex);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void updateContent() {
		DateFormatSymbols symbols = new DateFormatSymbols(calendarView.localeProperty().get());
		String[] monthNames = symbols.getShortMonths();
		for (int i = 1; i < monthNames.length; i++) {
			Button button = (Button) getChildren().get(i - 1);
			button.setText(monthNames[i - 1]);
		}
		title.set("V�lg �r;  "+getDateFormat("yyyy").format(calendarView.getCalendar().getTime()));
		//  title.set("V�lg �r");

	}
}
