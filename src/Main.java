
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.joda.time.DateTime;
import com.sun.javafx.css.Stylesheet;


import DatePicker.DatePicker;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
/**
 * This Class is the GUI class, it creates a user interface for the user to view.
 * The main components of this application is for the user to view the graph
 * please view the create Center and create lineChat methods to understand how the graph and table
 * is created and connected.
 * @author MRCR
 *
 */
@SuppressWarnings("rawtypes")
public class Main extends Application{
	private Statistics statistic = new Statistics();
	private DatePicker dp2;
	private AnchorPane r;
	private String choice = "" ;
	private CheckBox secoundaryWithWeekend;
	private Tab besvarelsesProcent = new Tab("Svarprocent");
	private Tab GnsAntalKald = new Tab("Antal Kald");
	private Tab akkumuleretSvarprocent = new Tab("Akkumuleret Svarprocent");
	private CheckBox weekend = new CheckBox();
	public Date today = new Date();
	public CheckBox compareData;
	public Button btn_HentData;
	private ArrayList<DateTime> secondDates = new ArrayList<DateTime>();
	public LineChart<String, Number> antalKaldChart;
	public LineChart<String, Number> procentChart;
	public LineChart<String, Number> akkumuleretChart;
	public LineChart<String, Number> lineChart;
	public ArrayList<DateTime> dates = new ArrayList<DateTime>();
	public TableView<XYChart.Series<String,Number>> antalKaldTable;
	public TableView<XYChart.Series<String,Number>> procentTable;
	public TableView<XYChart.Series<String,Number>> akkumuleretTable;
	public ObservableList data;
	public TableView<XYChart.Series<String,Number>> table;
	public DatePicker datePicker;
	private boolean withWeekend;
	public BorderPane bp;
	public TableColumn legendCol;
	private boolean secondaryWithWeekend;
	private final CheckBox prognoseChoice = new CheckBox();
	private final CheckBox secondPrognoseChoice = new CheckBox("Vis Prognose");
	private DateTime compareStart;
	private boolean secondDay;
	private boolean secondWeek;
	private TableCreator tc = new TableCreator();
	private boolean secondMonth;
	private final static  String kvaterTime[] ={"8:00","8:15","8:30", "8:45", "9:00", "9:15", "9:30",
		"9:45", "10:00","10:15","10:30","10:45","11:00",
		"11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30",
		"14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00",
		"18:15","18:30","18:45","19:00","19:15","19:30","19:45"};
	public final ObservableList<String> combobox_data = FXCollections.observableArrayList("Alle Telefonk�er");
	public final ObservableList<String> PeriodSelction = FXCollections.observableArrayList("Dag","Uge","M�ned");
	public static void main(String[] args){
		launch(args);
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		bp = new BorderPane();
		bp.setTop(createTopPane());
		Group root = new Group();
		root.getChildren().add(bp);
		bp.setCenter(createCenter());
		Scene scene = new Scene(root);
		scene.getStylesheets().addAll("test.css", "calendarstyle.css");
		primaryStage.setTitle("Telefon henvendelser");
		primaryStage.setScene(scene);
		primaryStage.setMinHeight(600);
		primaryStage.setResizable(false);
		primaryStage.setMinWidth(650);
		primaryStage.getIcons().add(new Image("NykreditLogo.jpg"));
		primaryStage.show();
	}
	/**
	 * Creates the center pane this is pane will fill out the rest of the GUI.
	 * The pane contains the Graph and the table
	 * @return
	 */
	private Node createCenter() {;
	TabPane tp = new TabPane();
	tp.setId("MyTabPane");
	tp.getTabs().add(GnsAntalKald);
	tp.getTabs().add(besvarelsesProcent);
	tp.getTabs().add(akkumuleretSvarprocent);
	GnsAntalKald.setClosable(false);
	besvarelsesProcent.setClosable(false);
	akkumuleretSvarprocent.setClosable(false);
	Date d = new Date();
	
	
			
	d.setDate(d.getDate()-1);
	DateTime endDates = new DateTime(d);
	DateTime startDate = new DateTime(d);
	dates.add(startDate);
	dates.add(endDates);
	System.out.println(d);
	GnsAntalKald.setContent(createLineChart("Gns Antal Kald","Series 1",startDate,endDates,true));
	besvarelsesProcent.setContent(createLineChart("Procent","Series 1", startDate, endDates,true));
	akkumuleretSvarprocent.setContent(createLineChart("Akkumuleret", "Series 1", startDate, endDates,true));
	for (int i = 0; i < tp.getTabs().toArray().length; i++) {
		createMouseEventForTab(tp.getTabs().get(i));

	}
	return 	tp;
	}
	private void createMouseEventForTab(final Tab n){
		MenuItem item = new MenuItem("Kopier "+n.getText().toString());
		final ContextMenu menu = new ContextMenu();
		item.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				WritableImage image = n.getContent().snapshot(new SnapshotParameters(), null); 
				ClipboardContent cc = new ClipboardContent();
				cc.putImage(image);
				Clipboard.getSystemClipboard().setContent(cc);
				// TODO: probably use a file chooser here 
				System.out.println("File saved");
				File file = new File("testing.png");  
				try {  ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);  } 

				catch (IOException e) { 
					// TODO: handle exception here  } }  
				}

			}
		});
		menu.getItems().add(item);
		n.getContent().addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getButton() == MouseButton.SECONDARY) {
					double initialX = event.getScreenX();
					double initialY = event.getScreenY();
					menu.show(n.getContent(), initialX-n.getContent().getScene().getX(), initialY-n.getContent().getScene().getY());	
				}else {
					menu.hide();
				}
				


			}
		});

		n.setContextMenu(menu);

	}

	/**
	 * This method creates the lineChart. The chart hBox is then returned
	 * so that it is able to be added onto any pane
	 * @author MRCR
	 * @param name
	 * @param endDates 
	 * @param startDate 
	 * @return Hbox layout
	 */
	@SuppressWarnings("unchecked")
	private Node createLineChart(String name,String seriesName, DateTime startDate, DateTime endDates, boolean withWeekend) {
		System.out.println("end date "+endDates);
		final CategoryAxis xAxis = new CategoryAxis();
		xAxis.setTickLength(0);
		final NumberAxis yAxis = new NumberAxis();
		if (name.equalsIgnoreCase("Gns Antal Kald")) {
			antalKaldChart = new LineChart(xAxis, yAxis);
			lineChart = antalKaldChart;
		}else if (name.equalsIgnoreCase("Procent")) {
			procentChart = new LineChart(xAxis, yAxis);
			lineChart = procentChart;
		}else {
			akkumuleretChart = new LineChart(xAxis, yAxis);
			lineChart = akkumuleretChart;
		}
		//	lineChart = new LineChart(xAxis, yAxis);
		lineChart.setStyle("-fx-padding: 0;");
		lineChart.setLegendVisible(false);
		lineChart.setTitle("Telefon Henvendelser");
		lineChart.getData().addAll(createSeries(name,seriesName,startDate,endDates, withWeekend));
		VBox vbox = new VBox(5);
		vbox.getChildren().addAll(lineChart, createTableView(lineChart,name));
		vbox.setAlignment(Pos.TOP_LEFT);
		vbox.setMinWidth(500);
		// previously 114
		VBox.setMargin(lineChart, new Insets(0,0,0,20));
		HBox layout = new HBox();
		HBox.setHgrow(vbox, Priority.ALWAYS);
		layout.getChildren().add(vbox);

		return layout;
	}
	/**
	 * 	 * This method creates the data contained within the lineChart this data is later used to populated each tableColum in the tableView
	 * @param name
	 * @param startDate
	 * @param endDate
	 * @return series
	 */
	@SuppressWarnings("unchecked")
	private Series createSeries(String name,String seriesName, DateTime startDate, DateTime endDate, boolean withWeekend) {
		XYChart.Series series = new XYChart.Series();
		series.setName(seriesName);
		data = series.getData();
		int i = 0;		
		if(name.equalsIgnoreCase("Gns Antal Kald")) {
			ArrayList<Double>averageData = statistic.getCallAmountData(startDate, endDate,name, withWeekend);	
			if (!averageData.isEmpty()) {
				for (String time : kvaterTime) {
					if (averageData.get(i) == null) {
						data.add(new XYChart.Data(time, 0));
						i++;
					}else {
						data.add(new XYChart.Data(time, Math.round(averageData.get(i))));
						i++;		
					}
				}		
			}
		}else if (name.equalsIgnoreCase("Procent")) {
			ArrayList<Double>averageData = statistic.getCallPercentageData(startDate, endDate, name, withWeekend, 0);
			if (!averageData.isEmpty()) {
				System.out.println(averageData);
				for (String time : kvaterTime) {
					if (averageData.get(i) == null) {
						data.add(new XYChart.Data(time, 0));
						i++;
					}else {
						data.add(new XYChart.Data(time, Math.round(averageData.get(i))));
						i++;		
					}
				}		
			}
		}else if (name.equalsIgnoreCase("Akkumuleret"))  {
			ArrayList<Double>akkumuleret = statistic.getAkkumuleretSvarprocent();
			if (!akkumuleret.isEmpty()) {
				for (String time : kvaterTime) {
					if (akkumuleret.get(i) == null) {
						data.add(new XYChart.Data(time, 0));
						i++;
					}else {
						data.add(new XYChart.Data(time, Math.round(akkumuleret.get(i))));
						i++;		
					}
				}		
			}
		}else {
			ArrayList<Double>forecast = statistic.getForecast(startDate, endDate, withWeekend);
			if (!forecast.isEmpty()) {
				for (String time : kvaterTime) {
					if (forecast.get(i) == null) {
						data.add(new XYChart.Data(time, 0));
						i++;
					}else {
						data.add(new XYChart.Data(time, Math.round(forecast.get(i))));
						i++;		
					}
				}		
			}
		}
		return series;
	}

	/**
	 * This method sets the data of the TableView the data is gained from the LineChart's series
	 * using the series.getData()
	 * The tableView is then returned so it can be added to the Hbox containing the Graph and the Table
	 * @param chart
	 * @param name 
	 * @return TableView
	 */
	@SuppressWarnings("unchecked")
	private TableView createTableView(final LineChart<String, Number> chart, String name) {
		if (name.equalsIgnoreCase("Gns Antal Kald")) {
			antalKaldTable = new TableView<>();
			antalKaldChart.setId("my-table");
			return tc.createIndividualTable(chart, antalKaldTable);
		}else if (name.equalsIgnoreCase("Procent")) {
			procentTable = new TableView<>();
			procentTable.setId("my-table");
			return tc.createIndividualTable(chart, procentTable);
		}else {
			akkumuleretTable = new TableView<>();
			akkumuleretTable.setId("my-table");
			return tc.createIndividualTable(chart, akkumuleretTable);
		}


	}

	/**
	 * This method creates the symbol contained in the table showing the color of the line created!
	 * @param series
	 * @param seriesIndex
	 * @return symbol
	 */
	private Node createSymbol(Series<String, Number> series, int seriesIndex) {
		Node symbol = new StackPane();   
		symbol.getStyleClass().setAll(    
				"chart-line-symbol",    
				"series" + seriesIndex,   
				"default-color" + (seriesIndex % 8)   
				);    return symbol;
	}  

	/**
	 * This method creates the topPane and all that it contains this pane is the main control pane 
	 * and therefore it contains all of the functionality that the user have to manipulate the graph and table
	 * the method returns an Anchorpane that can be added to the Stage
	 * @return AnchorPane
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	public AnchorPane createTopPane(){
		withWeekend = true;
		Label headLine = new Label("Serie 1");
		headLine.setLayoutX(80);
		headLine.setLayoutY(2);
		final AnchorPane topPane = new AnchorPane();
		topPane.setId("AnchorPane");
		topPane.setPrefSize(820, 120);
		this.datePicker = new DatePicker();
		Date d = new Date();
		datePicker.removeYear();
		d.setDate(d.getDate()-1);
		this.datePicker.setSelectedDate(d);
		datePicker.getCalendarView().setShowWeeks(true);
		datePicker.getCalendarView().getCalendar().setTime(d);
		datePicker.selectedDateProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable arg0) {
				if (datePicker.getSelection().equalsIgnoreCase("dag")) {
					weekend.setDisable(true);
				}else {
					weekend.setDisable(false);
				}
				dates = datePicker.getSelectedDates();
			}
		});	
		HBox hBox = new HBox();
		Label l = new Label("V�lg Dato: ");
		hBox.setLayoutX(10);
		hBox.setLayoutY(25);
		hBox.setSpacing(10);
		hBox.getChildren().addAll(l,datePicker);
		datePicker.localeProperty();
		datePicker.setLocale(Locale.ROOT.UK);
		datePicker.getCalendarView().setTodayButtonText("Dags Dato");
		compareData = new CheckBox();
		compareData.setText("Samlign med");
		compareData.setLayoutX(320);
		compareData.setLayoutY(20);
		topPane.getChildren().add(createAdditionalChoices());
		r.setVisible(false);
		compareData.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				r.setVisible(newValue);
			}});

		weekend.setSelected(true);
		weekend.setDisable(true);
		weekend.setLayoutX(80);
		weekend.setLayoutY(75);
		prognoseChoice.setLayoutY(95);
		prognoseChoice.setText("Vis Prognose");
		prognoseChoice.setLayoutX(80);
		
		weekend.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				withWeekend = newValue;

			}
		});
		weekend.setText("Medtag weekend");
		btn_HentData = new Button();
		btn_HentData.setPrefSize(119, 37);
		btn_HentData.setLayoutX(687);
		btn_HentData.setLayoutY(35);
		btn_HentData.setId("hentData");
		btn_HentData.setText("Hent Data");
		btn_HentData.setCursor(Cursor.OPEN_HAND);
		btn_HentData.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				/*
				 * Makes sure that one of the checkboxes has been pressed
				 */
				DateTime startDate;
				DateTime endDate;
					if (dates.isEmpty()) {
						 startDate = new DateTime();
						endDate = new DateTime();
					}
					 startDate = dates.get(0);
					 endDate = dates.get(1);
					String k�Valg = "Gns Antal Kald";
					//lineChart.getData().clear();
					//	table.getItems().clear()
					GnsAntalKald.setContent(createLineChart(k�Valg, "Series 1", startDate, endDate, withWeekend));
					besvarelsesProcent.setContent(createLineChart("Procent", "Series 1",startDate, endDate,withWeekend));
					akkumuleretSvarprocent.setContent(createLineChart("Akkumuleret", "Series 1", startDate, endDate,withWeekend));
					if (prognoseChoice.isSelected()) {
						addDataToChartAndTable("Prognose", "Prog", startDate, endDate, withWeekend);
					}
					if (compareData.isSelected()) {
						compareStart = secondDates.get(0);
						DateTime compareEnd = secondDates.get(1);
						addDataToChartAndTable(k�Valg, "Series 2", compareStart, compareEnd, secondaryWithWeekend);
						addDataToChartAndTable("Procent", "Series 2", compareStart, compareEnd, secondaryWithWeekend);
						addDataToChartAndTable("Akkumuleret", "Series 2", compareStart, compareEnd, secondaryWithWeekend);
						if (secondPrognoseChoice.isSelected()) {
							addDataToChartAndTable("Prognose S2","2.Prog" , compareStart, compareEnd, secondaryWithWeekend);
						}
					}
					createMouseEventForTab(GnsAntalKald);
					createMouseEventForTab(besvarelsesProcent);
					createMouseEventForTab(akkumuleretSvarprocent);
				}
		
			
		});
		topPane.getChildren().addAll(compareData,hBox, weekend,prognoseChoice,btn_HentData,headLine);
		return topPane;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This method removes the current data from the chart and the table.
	 * After removing the old data it then adds the new information to both the Chart and the table
	 * @param series
	 */
	public void updateChatAndTable(LineChart chart, DateTime startDate, DateTime endDate){
		if (chart.getData().size() > 0) {
			for (int i = 0; i < chart.getData().size(); i++) {
				chart.getData().remove(i);
				table.getItems().clear();
			}
		}
		lineChart.getData().add(createSeries("Gns Antal kald", "Series 1", startDate, endDate,withWeekend));
		table.getItems().add(createSeries("Gns Antal kald", "Series 1", startDate, endDate,withWeekend));
	}
	public void addNewDataToChart(Series series){
		lineChart.getData().add(series);
		table.getItems().add(series);
	}
	/**
	 * This method calculates the desired endDate depending on what type of input the user has provided
	 * @param startDate2
	 * @return
	 */
	@SuppressWarnings("static-access")

		/*	if (Selection == 0) {
		}else if (week.isSelected()) {
			System.out.println("Startdate" +startDate2);
			DateTime endDatoWeek = endDato.plusDays(+7);
			System.out.println("New end" +endDatoWeek);
			return endDatoWeek;
		}else if (month.isSelected()) {
			DateTime endDatoMonth = endDato.dayOfMonth().withMaximumValue();
			return endDatoMonth;
		}
		 */
	/**
	 * This private method updates the comboBox choice depending on available list stored in the application
	 * @deprecated
	 */
	private void updateBoxChoice() {
	}
	private void createNewLineAndTable(String type,String name, DateTime startDate, DateTime endDate, boolean weekends) {
		XYChart.Series series = new XYChart.Series();
		ObservableList data2 = series.getData();
		ArrayList<Double>averageData;
		//	data = series.getData();
		int i = 0;
		System.out.println(type);
		if(type.equalsIgnoreCase("Gns Antal Kald")) {
			averageData= statistic.getCallAmountData(startDate, endDate, type, weekends);
		}else if (type.equalsIgnoreCase("Procent")) {

			averageData= statistic.getCallPercentageData(startDate, endDate, type, weekends,0);
		}else {
			averageData = statistic.getAkkumuleretSvarprocent();
		}
		for (String time : kvaterTime) {
			if (averageData.get(i) == null) {
				data2.add(new XYChart.Data(time, 0));
				i++;
			}else {
				data2.add(new XYChart.Data(time, Math.round(averageData.get(i))));
				i++;	
			}
		}
		series.setName(name);
		lineChart.getData().add(series);
		table.getItems().add(series);
	}
	@SuppressWarnings("unused")
	/*private Series updateChartSeries(String name, String type) {

		XYChart.Series series = new XYChart.Series();
		series.setName(name);
		data = series.getData();
		ArrayList<Double> averageData = statistic.getCallAmountData(name,type);
		int i = 0;
		for (String time : TIME) {
			data.add(new XYChart.Data(time, Math.round(averageData.get(i))));
			i++;
		}
		return series ;
	}
	 */
	/**
	 * This static method is used to generate a popup error message using the String errorMessage 
	 * a new window is created containing the text a warning sign and a button
	 * @param errorMessage
	 */
	static void createErrorMessage(String errorMessage) {
		Button confirm = new Button();
		confirm.setLayoutX(360);
		confirm.setCursor(Cursor.OPEN_HAND);
		confirm.setText("Ok");
		confirm.setId("errorButton");

		Label errorIcon = new Label();
		errorIcon.setPrefSize(47,47);
		errorIcon.setId("errorIcon");

		Label message = new Label(errorMessage);
		message.setId("errorMessage");	

		final Stage errorStage = new Stage();
		Group r = new Group();
		Scene s = new Scene(r);
		errorStage.initModality(Modality.WINDOW_MODAL);
		errorStage.setScene(s);

		GridPane gp = new GridPane();
		gp.add(errorIcon,1,1);
		gp.add(message, 2, 1);

		BorderPane bp = new BorderPane();
		bp.setCenter(gp);

		AnchorPane bottum = new AnchorPane();
		bottum.setId("errorPane");
		bottum.getChildren().add(confirm);
		bp.setBottom(bottum);

		r.getChildren().add(bp);
		errorStage.setResizable(false);
		errorStage.getScene().getStylesheets().add("test.css");
		confirm.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				errorStage.close();	
			}
		});
		errorStage.setTitle("Fejl!");
		errorStage.show();
	}

	@SuppressWarnings("unchecked")
	private Node createAdditionalChoices() {
		r = new AnchorPane();
		HBox dateBox = new HBox();
		Label l = new Label("V�lg Dato: ");
		secondaryWithWeekend = true;
		dp2 = new DatePicker();
		dp2.removeYear();
		dp2.getCalendarView().setShowWeeks(true);
		dateBox.getChildren().addAll(l,dp2);
		dp2.selectedDateProperty().addListener(new InvalidationListener() {

			@Override
			public void invalidated(Observable observable) {
				if (!secondDates.isEmpty()) {
					secondDates.clear();
				}
				if (dp2.getSelection().equalsIgnoreCase("Dag")) {
					secoundaryWithWeekend.setDisable(true);
				}else {
					secoundaryWithWeekend.setDisable(false);
				}
				secondDates = dp2.getSelectedDates();
				compareStart = secondDates.get(0);
			}
		});
		Label l2 = new Label();
		HBox periodBox = new HBox();


		secoundaryWithWeekend = new CheckBox("Medtag Weekend");
		secoundaryWithWeekend.setSelected(true);
		secoundaryWithWeekend.setDisable(true);
		secoundaryWithWeekend.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				secondaryWithWeekend = newValue;
			}
		});
		dateBox.setSpacing(5);

		VBox mainBox = new VBox();
		mainBox.setSpacing(5);
		Label pushingLabel = new Label();
		Label pushingLabel2 = new Label();
		HBox topping = new HBox();
		Label headLineName = new Label("Serie 2");
		topping.getChildren().addAll(new Label(), headLineName);
		topping.setSpacing(64);
		HBox lowerBox = new HBox();
		lowerBox.getChildren().addAll(pushingLabel, secoundaryWithWeekend);
		lowerBox.setSpacing(64);
		HBox buttomBox = new HBox();
		buttomBox.setSpacing(64);
		buttomBox.getChildren().addAll(pushingLabel2, secondPrognoseChoice);
		mainBox.getChildren().addAll(periodBox,dateBox,lowerBox, buttomBox);
		mainBox.setLayoutY(16);
		topping.setLayoutY(0);
		r.setLayoutX(450);
		r.setLayoutY(0);
		r.getChildren().addAll(topping, mainBox);
		return r;
	}
	public void addDataToChartAndTable(String name, String seriesName, DateTime start, DateTime end,boolean withWeekend  ){
		if (name.equalsIgnoreCase("Gns antal kald")) {
			XYChart.Series series = createSeries(name, seriesName, start, end,withWeekend);
			antalKaldChart.getData().add(series);
			antalKaldTable.getItems().add(series);
		}else if (name.equalsIgnoreCase("Procent")) {
			XYChart.Series series = createSeries(name, seriesName, start, end,withWeekend);
			procentChart.getData().add(series);
			procentTable.getItems().add(series);
		}else if (name.equalsIgnoreCase("Akkumuleret")) {
			XYChart.Series series = createSeries(name, seriesName, start, end,withWeekend);
			akkumuleretChart.getData().add(series);
			akkumuleretTable.getItems().add(series);
		}else {
			XYChart.Series series = createSeries(name, seriesName, start, end,withWeekend);
			antalKaldChart.getData().add(series);

			antalKaldTable.getItems().add(series);
		}
	}
	public static String[] getTimeArray(){
		return kvaterTime;
	}

}