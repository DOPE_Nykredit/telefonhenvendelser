import java.util.HashMap;
/**
 * This object is used to contain data gained from the database.
 * The object is contained and created in the Provider class
 * @author MRCR
 *
 */
public class CallQueue {
	private String type;
	private HashMap<Integer, Integer> data = new HashMap<Integer,Integer>();
	public CallQueue(String type){
		this.type = type;
	}
	
	/**
	 * This method returns the name of the object
	 * @return {@link String}
	 */
	public String getType(){
		return type;
	}
	/**
	 * This method returns the data for each time
	 * @param time
	 * @return {@link Integer}
	 */
	public int getCallsByTime(int time){
		if (data.get(time) == null) {
			data.put(time, 0);
		}
		return data.get(time);
		
	}
	public void addCallsByTime(int hourOfDay, int callAmount) {
		if (data.get(hourOfDay) != null ) {
			int i = data.get(hourOfDay) + callAmount;
			data.put(hourOfDay, i);
		}else {
			data.put(hourOfDay, callAmount);
				
		}
		
	}

}
